# PUV, TS Part (Exercise 3 & 4)


## Setup:

1. Create your venv (python 3.10) with `python -m venv venv`
2. Activate your venv `.\venv\Scripts\activate`
3. Install openbus_light as you find it here `pip install openbus_light-X.X.X-py3-none-any.whl`
4. Check if everything works by running the unittests: `python -m unittest`
5. Open any IDE of your choice and start working on `exercise_3.py` and `exercise_4.py`


