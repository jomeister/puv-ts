from typing import NamedTuple


class ScenarioPaths(NamedTuple):
    to_demand: str
    to_lines: str
    to_stations: str
    to_districts: str
    to_measurements: str
