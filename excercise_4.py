from collections import Counter
import os
import pickle
import tempfile

import pandas as pd
from excercise_3 import configure_parameters, load_paths
from pandas import DataFrame
from matplotlib import pyplot as plt
import numpy as np
import datetime

from openbus_light.manipulate import load_scenario
from openbus_light.manipulate.recorded_trip import enrich_lines_with_recorded_trips
from openbus_light.model import BusLine, RecordedTrip


def calculate_trip_times(recorded_trip: RecordedTrip) -> pd.DataFrame:
    trip_time_planned = (
        arrival - departure
        for departure, arrival in zip(
            recorded_trip.record["departure_planned"][:-1], recorded_trip.record["arrival_planned"][1:]
        )
    )
    trip_time_observed = (
        arrival - departure
        for departure, arrival in zip(
            recorded_trip.record["departure_observed"][:-1], recorded_trip.record["arrival_observed"][1:]
        )
    )
    return pd.DataFrame({"trip_time_planned": trip_time_planned, "trip_time_observed": trip_time_observed})


def calculate_dwell_times(recorded_trip: RecordedTrip) -> DataFrame:
    dwell_time_planned = (
        departure - arrival
        for arrival, departure in zip(
            recorded_trip.record["arrival_planned"][1:-1], recorded_trip.record["departure_planned"][1:-1]
        )
    )
    dwell_time_observed = (
        departure - arrival
        for arrival, departure in zip(
            recorded_trip.record["arrival_observed"][1:-1], recorded_trip.record["departure_observed"][1:-1]
        )
    )
    return pd.DataFrame({"dwell_time_planned": dwell_time_planned, "dwell_time_observed": dwell_time_observed})


def load_bus_lines_with_measurements(selected_line_numbers: frozenset[int]) -> tuple[BusLine, ...]:
    cache_key = "$".join(map(str, sorted(selected_line_numbers)))
    cache_filename = os.path.join(tempfile.gettempdir(), ".open_bus_light_cache", f"{cache_key}.pickle")
    if os.path.exists(cache_filename):
        with open(cache_filename, "rb") as f:
            print(f"loaded bus lines from cache {cache_filename}")
            return pickle.load(f)
    paths = load_paths()
    parameters = configure_parameters()
    baseline_scenario = load_scenario(parameters, paths)
    baseline_scenario.check_consistency()
    selected_lines = {line for line in baseline_scenario.bus_lines if line.number in selected_line_numbers}
    lines_with_recordings = enrich_lines_with_recorded_trips(paths.to_measurements, selected_lines)
    os.makedirs(os.path.dirname(cache_filename), exist_ok=True)
    with open(cache_filename, "wb") as f:
        pickle.dump(lines_with_recordings, f)
    return lines_with_recordings
    
def line_plot(planned_tt, observed_tt, titel, name, nummer, ticks, labels):
    plt.figure(nummer)
    median_planned = np.median(planned_tt, axis=0)
    median_observed = np.median(observed_tt, axis=0)
    perc_85_observed = np.percentile(observed_tt, q = 85 ,axis = 0, interpolation = "nearest")

    plt.plot(median_planned, label = "Median der geplanten Reisezeit", color = "r")
    plt.plot(median_observed, label = "Median der beobachteten Reisezeit")
    plt.plot(perc_85_observed, label = "P85 der beobachteten Reisezeit", linestyle = "dotted", color = "c")
    plt.xticks(ticks, labels)
    plt.legend()
    plt.title(titel)
    plt.savefig(name)

def line_plot_diff(planned_tt, observed_tt, titel, name, nummer, ticks, labels):
    plt.figure(nummer)

    diff_all = observed_tt - planned_tt
    median_diff = np.median(diff_all, axis = 0)
    diff_85 = np.percentile(diff_all, q = 85, axis = 0)

    plt.plot(median_diff, label = "Differenz der beobachteten zur geplanten Reisezeit")
    plt.plot(diff_85, label = "P85 der Differenz der beobachteten zur geplanten Reisezeit", linestyle = "dotted", color = "c")
    plt.plot()
    plt.legend()
    plt.title(titel)
    plt.xticks(ticks, labels)
    plt.savefig(name)

def boxplot(observed_tt_a, observed_tt_b, titel, name, nummer):
    total_tt_a = np.sum(observed_tt_a, axis = 1)
    total_tt_b = np.sum(observed_tt_b, axis=1)
    total_tt = {"Richtung A": total_tt_a, "Richtung B": total_tt_b}
    plt.figure(nummer)
    plt.boxplot(total_tt.values(), labels=total_tt.keys())
    plt.title(titel)
    plt.savefig(name)

def barplot(names, values, nummer):
    plt.figure(nummer)
    plt.bar(names, values)
    plt.title("Gesamte Anzahl Fahrten pro Stunde")
    plt.savefig("HZV_Analyse.jpg")

def cummulative_tt(planned_tt, observed_tt, titel, name, nummer, ticks, labels):
    plt.figure(nummer)
    median_planned = np.median(planned_tt, axis=0)
    median_observed = np.median(observed_tt, axis=0)
    perc_85_observed = np.percentile(observed_tt, q = 85 ,axis = 0)
    cumsum_planned = np.cumsum(median_planned)
    cumsum_observed = np.cumsum(median_observed)
    cumsum_85_observed = np.cumsum(perc_85_observed)
    plt.plot(cumsum_observed, label = "Median der beobachteten Reisezeiten")
    plt.plot(cumsum_planned, label = "Median der geplanten Reisezeit", color = "r")
    plt.plot(cumsum_85_observed, label = "P85 der beobachteten Reisezeiten", color = "c",linestyle = "dotted" )
    plt.plot()
    plt.legend()
    plt.title(titel)
    plt.xticks(ticks,labels)
    plt.savefig(name)

def cummulative_delay(planned_tt, observed_tt, title, name, nummer, ticks, labels):
    plt.figure(nummer)
    diff_all = observed_tt - planned_tt
    median_diff = np.median(diff_all, axis = 0)
    diff_85 = np.percentile(diff_all, q = 85, axis = 0)
    median_diff_cumm = np.cumsum(median_diff)
    diff_85_cumm = np.cumsum(diff_85)
    plt.plot(median_diff_cumm, label = "Median der beobachteten Reisezeiten")
    plt.plot(diff_85_cumm, color = "c", linestyle = "dotted", label = "P85 der beobachteten Reisezeiten")
    plt.legend()
    plt.title(title)
    plt.xticks(ticks, labels)
    plt.savefig(name)

def analysis(selected_line_numbers: frozenset[int]) -> None:
    lines_with_recordings = load_bus_lines_with_measurements(selected_line_numbers)
    all_dep_times = []
    for line in lines_with_recordings:        
        
        for direction in (line.direction_a, line.direction_b):

            planned_trip_times = np.empty([0,37])
            observed_trip_times = np.empty([0,37])
            
            for record in direction.recorded_trips:
                length = len(record.record.index)
                if length == 38: #trip auslassen, welche nicht 37 Halte beinhalten (genug kleiner Anteil, kann vernachlässigt werden)
                    dep_time = record.record["departure_planned"][0].hour #take hour out of timestamp
                    all_dep_times.append(dep_time)
                    arr_time = record.record["arrival_planned"][37].hour #take hour out of timestamp
                    filter = [6,7,8,15, 16, 17, 18, 19] #Hauptverkehrszeiten
                    stations_b = record.record["station_name"]

                    Total_tt = (record.record["arrival_observed"][37]-record.record["departure_observed"][0]).total_seconds()/60

                    if dep_time in filter and arr_time in filter and Total_tt >= 35: #nach Hauptverkehrszeiten und Reisezeit filtern
                        trip_times = calculate_trip_times(record) #Tuple mit allen Reisezeiten für Richtung (observed und planned)
                        
                        dwell_times = calculate_dwell_times(record)
                        #Zeiten von timestamp in float umwandeln
                        planned_times = []
                        observed_times = []

                        for i in range(37):
                            if i < 36:
                                planned_times.append(trip_times.iloc[i,0].total_seconds()/60 + dwell_times.iloc[i,0].total_seconds()/60) #Zeiten in Minuten als float
                                observed_times.append(trip_times.iloc[i,1].total_seconds()/60 + dwell_times.iloc[i,1].total_seconds()/60) #Zeiten in Minuten als float
                            else:
                                planned_times.append(trip_times.iloc[i,0].total_seconds()/60) #Zeiten in Minuten als float
                                observed_times.append(trip_times.iloc[i,1].total_seconds()/60) #Zeiten in Minuten als float


                        planned_trip_times = np.append(planned_trip_times, np.array([planned_times]), axis=0)
                        observed_trip_times = np.append(observed_trip_times, np.array([observed_times]), axis=0)


            #trip_times.plot()
            direction_name = direction.name
            if direction_name == "a":
                a_planned_trip_times = planned_trip_times
                a_observed_trip_times = observed_trip_times
            else:
                b_planned_trip_times = planned_trip_times
                b_observed_trip_times = observed_trip_times

    #Berechne Häufigkeit der Abfahrtszeiten
    occurance = Counter(all_dep_times)
    names = occurance.keys()
    values = occurance.values()

    ticks_a = [-0.5,14.5,22.5,36.5]
    ticks_b = [-0.5,13.5,21.5,36.5]
    labels_a = ["Bahnhof Wülflingen", "Schöntal", "Obertor", "Elsau, Melcher"]
    labels_b = ["Elsau, Melcher", "Obertor", "Schöntal", "Bahnhof Wülflingen"]

    #Plots
    #line_plot(a_planned_trip_times, a_observed_trip_times,"Reisezeiten in Richtung A", "tt_a.jpg", 0, ticks_a, labels_a)
    #line_plot(b_planned_trip_times, b_observed_trip_times, "Reisezeiten in Richtung B","tt_b.jpg", 1, ticks_b, labels_b)
    #line_plot_diff(a_planned_trip_times, a_observed_trip_times, "Differenz der geplanten und beobachteten Reisezeit in Richtung A","delay_a.jpg", 2, ticks_a, labels_a)
    #line_plot_diff(b_planned_trip_times, b_observed_trip_times, "Differenz der geplanten und beobachteten Reisezeit in Richtung B", "delay_b.jpg", 3, ticks_b, labels_b)
    #boxplot(a_observed_trip_times, b_observed_trip_times, "Boxplot der beobachteten Reisezeiten",  "Boxplot_total_tt.jpg", 4)
    #barplot(names, values, 5)
    cummulative_tt(a_planned_trip_times, a_observed_trip_times, "Kumulierte Reisezeiten in Richtung A", "cumulative_tt_a.jpg",6, ticks_a, labels_a)
    cummulative_tt(b_planned_trip_times, b_observed_trip_times, "Kumulierte Reisezeiten in Richtung B", "cumulative_tt_b.jpg",7, ticks_b, labels_b)
    #cummulative_delay(a_planned_trip_times, a_observed_trip_times, "Kumulierte Verspätung in Richtung A", "cumulative_delay_a.jpg", 8, ticks_a, labels_a)
    #cummulative_delay(b_planned_trip_times, b_observed_trip_times, "Kumulierte Verspätung in Richtung B", "cumulative_delay_b.jpg", 9, ticks_b, labels_b)

if __name__ == "__main__":
    analysis(frozenset((7,))) #Busliniennummer eingeben




"""
Ideen:
- Plot für Morgen und Plot für Abend trennen
- boxplot für alle Reisezeiten (HVZ)
- Geschwindigkeiten plotten

Subset plotten: median_planned[15:28] ABER index beginnt dann bei 0

Abschnitte:
0 - 15
16 - 23
23- 37




Fragen
Aufgabe 1: Identifikation von Hauptverkehrszeiten? --> mit counter
Daten von planned sind komisch, wie damit arbeiten?
Dwell times in planned enthalten? In Planned nicht enthalten --> dazu rechnen? --> JA!
Wie lange soll Problemstelle sein?

"""