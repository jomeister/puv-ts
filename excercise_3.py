from types import MappingProxyType
import warnings
from dataclasses import replace
from datetime import timedelta
from typing import Mapping

from constants import (
    GPS_BOX,
    MEASUREMENTS,
    PATH_TO_DEMAND,
    PATH_TO_DEMAND_DISTRICT_POINTS,
    PATH_TO_LINE_DATA,
    PATH_TO_STATIONS,
    WINTERTHUR_IMAGE,
)
from openbus_light.model.line import BusLine
from plots import plot_available_vs_used_capacity_for_each_direction, plot_available_vs_used_capacity_per_link

from openbus_light.manipulate import ScenarioPaths, load_scenario
from openbus_light.model import PlanningScenario
from openbus_light.plan import LinePlanningNetwork, LinePlanningParameters, LPPData, create_line_planning_problem
from openbus_light.plot.demand import PlotBackground, create_plot
from openbus_light.utils.summary import create_summary


def load_paths() -> ScenarioPaths:
    return ScenarioPaths(
        to_lines=PATH_TO_LINE_DATA,
        to_stations=PATH_TO_STATIONS,
        to_districts=PATH_TO_DEMAND_DISTRICT_POINTS,
        to_demand=PATH_TO_DEMAND,
        to_measurements=MEASUREMENTS,
    )


def configure_parameters() -> LinePlanningParameters:
    return LinePlanningParameters(
        egress_time_weight=1.1, 
        period_duration=timedelta(hours=1),
        waiting_time_weight=1.5,
        in_vehicle_time_weight=1,
        walking_time_weight=2.2,
        dwell_time_at_terminal=timedelta(seconds=5 * 60),
        vehicle_cost_per_period=250, # ein Bus kostet 250 CHF/h
        vehicle_capacity=60,
        permitted_frequencies=(4, 10),
        demand_association_radius=500,
        walking_speed_between_stations=0.6, #m/s mit Umweg
        maximal_walking_distance=300,
        demand_scaling=0.1,
        maximal_number_of_vehicles=None,
    )


def update_frequencies(
    scenario: PlanningScenario, new_frequencies_by_line_nr: Mapping[int, tuple[int, ...]]
) -> PlanningScenario:
    updated_lines = []
    for line in scenario.bus_lines:
        updated_lines.append(line._replace(permitted_frequencies=new_frequencies_by_line_nr[line.number]))
    return scenario._replace(bus_lines=tuple(updated_lines))


def update_capacities(scenario: PlanningScenario, new_capacities_by_line_nr: Mapping[int, int]) -> PlanningScenario:
    updated_lines = []
    for line in scenario.bus_lines:
        updated_lines.append(line._replace(regular_capacity=new_capacities_by_line_nr[line.number]))
    return scenario._replace(bus_lines=tuple(updated_lines))


def ist_scenario(baseline_scenario: PlanningScenario) -> PlanningScenario:
    #Frequenz und Kapazität der heutigen Situation
    new_frequencies_by_line_id = {1: (8,), 2: (14,), 3: (8,), 4: (6,), 5: (4,), 7: (6,), 9: (4,), 10: (6,)} 
    new_capacities_by_line_id = {1: 0.85*220, 2: 0.85*151, 3: 0.85*151, 4: 0.85*84, 5: 0.85*154, 7: 0.85*154, 9: 0.85*84, 10: 0.85*84} 
    updated_scenario = update_capacities(baseline_scenario, new_capacities_by_line_id)
    return update_frequencies(updated_scenario, new_frequencies_by_line_id)


def optimization_optimum(baseline_scenario: PlanningScenario) -> PlanningScenario:
    # Optimale Lösung: Frequenz = 10, Kapazität = 84
    new_frequencies_by_line_id = {1: (8,), 2: (14,), 3: (8,), 4: (6,), 5: (4,), 7: (10,), 9: (4,), 10: (6,)} 
    new_capacities_by_line_id = {1: 0.85*220, 2: 0.85*151, 3: 0.85*151, 4: 0.85*84, 5: 0.85*154, 7: 0.85*84, 9: 0.85*84, 10: 0.85*84} 
    updated_scenario = update_capacities(baseline_scenario, new_capacities_by_line_id)
    return update_frequencies(updated_scenario, new_frequencies_by_line_id)


def optimization_ohne_21_22(baseline_scenario: PlanningScenario) -> PlanningScenario:
    #Haltestellen Schmidgasse und Stadthaus auslassen
    new_frequencies_by_line_id = {1: (8,), 2: (14,), 3: (8,), 4: (6,), 5: (4,), 7: (5,), 9: (4,), 10: (6,)} 
    new_capacities_by_line_id = {1: 0.85*220, 2: 0.85*151, 3: 0.85*151, 4: 0.85*84, 5: 0.85*84, 7: 0.85*154, 9: 0.85*84, 10: 0.85*84} 
    updated_scenario = update_capacities(baseline_scenario, new_capacities_by_line_id)

    line_of_interest = find_line_of_interest(updated_scenario)

    #direction a: Schmidgasse = 21, Stadthaus = 22
    direction_a = line_of_interest.direction_a
    new_trips_a = direction_a.trip_times[0:20] + (direction_a.trip_times[20] + direction_a.trip_times[21]+direction_a.trip_times[22]-timedelta(seconds=2 * 60) ,)+ direction_a.trip_times[23:]
    new_stations_a = direction_a.station_names[0:21] + direction_a.station_names[23:]
    new_direction_a = replace(direction_a, trip_times=new_trips_a, station_names=new_stations_a)
    line_of_interest = line_of_interest._replace(direction_a=new_direction_a)

    #direction b: Stadthaus = 15, Schmidgasse = 16
    direction_b = line_of_interest.direction_b
    new_trips_b = direction_b.trip_times[0:14] + (direction_b.trip_times[14] + direction_b.trip_times[15]+direction_b.trip_times[16]-timedelta(seconds=2 * 60) ,)+ direction_b.trip_times[17:]
    new_stations_b = direction_b.station_names[0:15] + direction_b.station_names[17:]
    new_direction_b = replace(direction_b, trip_times=new_trips_b, station_names=new_stations_b)
    line_of_interest = line_of_interest._replace(direction_b=new_direction_b)

    #in Scenario speichern
    raw_lines = tuple(line for line in updated_scenario.bus_lines if line.number!= line_of_interest.number)+ (line_of_interest, )
    updated_scenario = updated_scenario._replace(bus_lines = raw_lines)
    return update_frequencies(updated_scenario, new_frequencies_by_line_id)


def optimization_ohne_20_bis_23(baseline_scenario: PlanningScenario) -> PlanningScenario:
    #von Sulzer direkt nach Gewerbeschule fahren
    new_frequencies_by_line_id = {1: (8,), 2: (14,), 3: (8,), 4: (6,), 5: (4,), 7: (10,), 9: (4,), 10: (6,)} 
    new_capacities_by_line_id = {1: 0.85*220, 2: 0.85*151, 3: 0.85*151, 4: 0.85*84, 5: 0.85*84, 7: 0.85*84, 9: 0.85*84, 10: 0.85*84} 
    updated_scenario = update_capacities(baseline_scenario, new_capacities_by_line_id)
    line_of_interest = find_line_of_interest(updated_scenario)

    #direction a: HB = 20, Schmidgasse = 21, Stadthaus = 22, Obertor = 23
    direction_a = line_of_interest.direction_a
    new_trips_a = direction_a.trip_times[0:19] + (direction_a.trip_times[19] + direction_a.trip_times[20]+direction_a.trip_times[21]+direction_a.trip_times[22]+direction_a.trip_times[23]-timedelta(seconds=6 * 60) ,)+ direction_a.trip_times[24:]
    new_stations_a = direction_a.station_names[0:20] + direction_a.station_names[24:]
    new_direction_a = replace(direction_a, trip_times=new_trips_a, station_names=new_stations_a)
    line_of_interest = line_of_interest._replace(direction_a=new_direction_a)

    #direction b: Obertor = 14, Stadthaus = 15, Schmidgasse = 16, HB = 17
    direction_b = line_of_interest.direction_b
    new_trips_b = direction_b.trip_times[0:13] + (direction_b.trip_times[13] + direction_a.trip_times[14]+ direction_b.trip_times[15]+direction_b.trip_times[16]+direction_b.trip_times[17]-timedelta(seconds=6 * 60) ,)+ direction_b.trip_times[18:]
    new_stations_b = direction_b.station_names[0:14] + direction_b.station_names[18:]
    new_direction_b = replace(direction_b, trip_times=new_trips_b, station_names=new_stations_b)
    line_of_interest = line_of_interest._replace(direction_b=new_direction_b)
    
    #in Scenario speichern
    raw_lines = tuple(line for line in updated_scenario.bus_lines if line.number!= line_of_interest.number)+ (line_of_interest, )
    updated_scenario = updated_scenario._replace(bus_lines = raw_lines)
    return update_frequencies(updated_scenario, new_frequencies_by_line_id)

def do_the_line_planning(do_plot: bool) -> None:
    paths = load_paths()
    parameters = configure_parameters()
    baseline_scenario = load_scenario(parameters, paths)

    updated_scenario = optimization_ohne_20_bis_23(baseline_scenario)

    updated_scenario.check_consistency()
    planning_data = LPPData(
        parameters,
        updated_scenario,
        LinePlanningNetwork.create_from_scenario(updated_scenario, parameters.period_duration),
    )

    if do_plot:
        figure = create_plot(
            stations=planning_data.scenario.stations, plot_background=PlotBackground(WINTERTHUR_IMAGE, GPS_BOX)
        )
        figure.savefig("stations_and_caught_demand.jpg", dpi=900)

    lpp = create_line_planning_problem(planning_data)
    lpp.solve()
    result = lpp.get_result()

    if result.success:

        #Plot für jede Linie
        for line, passengers in result.solution.passengers_per_link.items():
            plot_available_vs_used_capacity_for_each_direction(line, passengers).savefig(
                f"available_vs_used_capacity_for_line_{line.number}.jpg", dpi=900
            )
        
        #Plots über alle Linien
        plot_available_vs_used_capacity_per_link(result.solution.passengers_per_link, sort_criteria="pax").savefig(
            "available_vs_used_capacity_sorted_by_pax.jpg", dpi=900
        )
        plot_available_vs_used_capacity_per_link(result.solution.passengers_per_link, sort_criteria="capacity").savefig(
            "available_vs_used_capacity_sorted_by_capacity.jpg", dpi=900
        )

        #Resultat ausgeben
        print(create_summary(planning_data, result))

        return
    warnings.warn(f"lpp is not optimal, adjust {planning_data.parameters}")

# suche nach Linie, welche bearbeitet werden soll
def find_line_of_interest(scenario: PlanningScenario) -> BusLine:
    return next(line for line in scenario.bus_lines if line.number == 7)


if __name__ == "__main__":
    do_the_line_planning(do_plot=False)


